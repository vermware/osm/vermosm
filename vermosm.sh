#!/bin/bash

set -e

shopt -s nullglob
shopt -s globstar

# version
VERMOSM_DATE='2020-12-02'
VERMOSM_DEV='vermware'
VERMOSM_URL='https://gitlab.com/vermware/osm/vermosm'
VERMOSM_VERSION='1.5.1'
VERMOSM_YEAR='2017-2020'

# load funcs and defaults
source vermosm/dem.sh
source vermosm/dep.sh
source vermosm/gmap.sh
source vermosm/install.sh
source vermosm/io.sh
source vermosm/opt.sh
source vermosm/osm.sh
source vermosm/poly.sh
source vermosm/routino.sh

# user config
[[ -e vermosm.cfg ]] && source vermosm.cfg

# opt parse
opt_parse "$@"
opt_print

# parallel
for i in {0..5}
do
	eval "unset p$i p${i}_pid"
	[[ $opt_parallel ]] && eval "p${i}='& p${i}_pid=\$!'"
done

# internal opt
# order matters, must follow deps
io_opt_set
poly_opt_set
osm_opt_set
dem_opt_set
routino_opt_set
gmap_opt_set
install_opt_set

# build dir
io_build_dir_mk
io_build_dir_clean

# dep
dep_opt_set
dep_gdalbuildvrt
dep_gpsbabel
dep_mkgmap
dep_planetsplitter
dep_splitter

# poly
poly_download
poly_merged_mk

# download/update everything first now that poly has been merged
# prevent 404 after waiting an hour this way
eval gmap_geonames_download $p0
eval gmap_sea_download $p1
eval osm_download $p2
dem_download

# depends on dem_download
# osm_download usually takes longer, especially when updating with diff
# dem_vrt_mk is virtually instant, writes only a text file
eval dem_mk $p3
dem_vrt_mk

# depends on osm_download
wait $p2_pid
[[ $opt_routino == 'region'* ]] && eval routino_db_mk $p2
eval osm_gpi_mk $p5
osm_extract_mk

# depends on osm_extract_mk
[[ $opt_routino != 'region'* ]] && eval routino_db_mk $p2
eval gmap_bounds_mk $p4

# depends on dem_mk
wait $p3_pid
dem_osm_merged_mk

# depends on dem_osm_merged_mk, gmap_geonames_download, gmap_sea_download
# gmap_licence_mk and gmap_typ_mk are virtually instant
wait $p0_pid
wait $p1_pid
gmap_licence_mk
gmap_typ_mk
gmap_tiles_mk

# depends on gmap_tiles_mk, gmap_bounds_mk
wait $p4_pid
gmap_gmapsupp_mk

# depends on routino_db_mk, osm_gpi_mk
wait $p2_pid
wait $p5_pid
install

# cleanup
msg "cleanup tmp files -> $opt_build_dir"
poly_rm
osm_gpi_rm
osm_extract_rm
dem_rm
dem_osm_merged_rm
gmap_bounds_rm
gmap_tiles_rm
gmap_typ_rm
gmap_licence_rm
gmap_gmapsupp_rm

# print success msg in green if stdout is tty
if [[ -t 1 && $(command -v tput) ]]
then
	msg '%s%s%s\n' \
		"$(tput setaf 2)" \
		'map generated successfully' \
		"$(tput sgr0)"
else
	msg 'map generated successfully'
fi

exit 0
