# vermosm

flexible OSM-based map generation

* Internal: https://gitlab.vpn.vermwa.re/osm/vermosm
* Public: https://gitlab.com/vermware/osm/vermosm

## Licence

> **Note:** Dependencies, downloaded data and other assets also have their
> licence. You have to comply to all licences at the same time. Generally
> speaking the built maps are "free for personal use" and may not be used
> commercially or sold. Sharing them for free however is okay.

vermosm is licensed under the GNU AGPLv3.
View `LICENCE` for details.

## End Users

> **Note:** For parallel, region-wide builds with all options around 100-200GB
> of disk space and up to 16-24GB of memory is required!

> **Note:** Routino version 3.2 and older have a bug in `nodesx.c:IndexNodeX()`
> and other places that causes overflow resulting in an infinite loop for
> regions that contain more than 2^31 nodes. To fix several binary search
> algorithms need to be updated, which is done upstream at revision 1968:
> http://www.routino.org/viewvc?view=revision&revision=1968. For regions with
> more than 2^31 nodes either compile r1968 or newer or wait for Routino 3.3.

For now `cd` to the repo's root directory and run `./vermosm.sh --help`. Note
that the working directory must be the repo's root or things will not work.

Proper documentation will arrive in the future, the output of `--help`:

```
$0 [OPTION]...
	vermosm / flexible OSM-based map generation

-b, --build-dir <dir>
	set build directory
	defaults to '$opt_build_dir'

-c, --country <country>
	country to build for
	specify multiple times to build for multiple countries
	all countries must be inside the specified region
	overrides --poly
	BUG cross-border routing has serious problems, prefer --poly (issue #12)

-C, --config <cfg>
	vermosm.cfg-style file to source
	overrides built-in defaults
	overrides system-wide vermosm.cfg options (stored next to vermosm.sh)
	overrides command-line options passed before, but not after, this option
	can be used multiple times in complex setups, above rules apply
	--config dump will print config and exit (pass as final option if used)

-d, --dem
	download and merge elevation data (DEM) as contour lines on the map
	on larger multi-country maps this is quite slow

-D, --dem-vrt
	merge all locally downloaded elevation data (DEM) into a single vrt
	the current region and country selection will be downloaded if needed
	all previous downloads in the same build directory will also be included

-g, --gpi
	generate a garmin-compatible gpi file containing all enforcement related
	nodes as waypoints suitable for the proximity/alert system
	generated from the full region, unless a single country is selected
	WARNING use of this file may not be legal in some countries

-h, --help
	print this message and exit

-i, --install <dir>
	install build files to <dir>
	files that already exist will be updated (overwritten)
	only selected options will be copied, dirs made if needed
	<dir>/dem/vermosm_dem_vrt.vrt and the VIEW3 folder
	<dir>/gpi/vermosm_*.gpi
	<dir>/map/vermosm_region_country..._gmapsupp.img
	<dir>/routino/vermosm_region_country...-*.mem
	<dir>/typ/vermosm_*.typ

-j, --jobs <jobs>
	number of jobs to run simultaneously in applications called by vermosm
	set to a low value if you run out of memory during build
	defaults to nproc which on this machine is '$opt_jobs'

-k, --keep
	keep *vermosm_* tmp files in the build directory
	they will be removed next build to prevent old data from being merged

-p, --poly <poly>
	poly file to build for, the name will be read from the file's first line
	the area specified in the polygon must be inside the specified region
	single-route gpx named routes supported, make sure extension is .gpx
	overrides --country

-P, --parallel
	run applications at the same time if possible, not related to --jobs
	for example, extracting osm region and dem contour lines run together
	this option may use up to two times as much memory as usual

-q, --quiet
	print only errors
	overrides --verbose

-r, --region <region>
	parent region for specified country or poly
	if neither of the above are specified build the entire region

-R, --routino <region|region-slim|selection|selection-slim>
	build a routino database for the specified region or selection
	*-slim uses much less memory but needs more time

-s, --style <style>
	file or directory to obtain map style from
	view mkgmap's --style-file option for more information
	defaults to '$opt_style'

-t, --typ <typ>
	typ file, in text form, to obtain extra bitmaps from
	view mkgmap's typ compiler manual and openstreetmap wiki for details
	defaults to '$opt_typ'

-T, --typ-bundled
	compile all vermosm bundled typ files in addition to --typ
	used in conjunction with --install to install all bundled typ files

-v, --verbose
	print what the script is doing
	overrides --quiet

-V, --version
	print version, copyright, licence information and where
	to find additional documentation and source code, then exit

--
	end of options
	any option after this is silently ignored
```

### Polygons

To use the GPX conversion feature of the `--poly` option:

* in software like QMapShack, create a dummy route around a portion of the
  region using online maps.
* make sure to name this route, for example `germany_trier`
* save this route as a GPX file, make sure it contains nothing else
* pass the file to vermosm, for example `--poly /path/to/germany_trier.gpx`

Several example GPX files are included in the `poly` folder.

![germany_trier](asset/doc/poly_gpx_germany_trier.png)

### Routino

Adjusted `profiles.xml` and `tagging.xml` can be found in the `routino`
directory. All modifications are commented with `vermeeren`. For the special
`destination-access` branch of Routino `tagging-destinationaccess.xml` should be
used.

* base xml updated to upstream r1998, containing several improvements
* forbid routing through destination-only ways (local traffic, anlieger frei)
	* the same change was made independently upstream in r1996
* routing of moped/motorcycle on tracks and paths (with very low preference)
* greatly increased preference for ferries for the motorcycle profile
* newly implemented ferry routing permission heuristic
	* merged upstream in r1970

To use: overwrite both of your system's Routino `profiles.xml` and `tagging.xml`
with the ones in the repository *before* generating maps.

## Dependencies

Thanks to all the projects listed below for their contributions to OSM-land!

### Required

* `bash` in a \*NIX environment
	* GNU/Linux
	* MSYS2 on Windows
	* OS X
	* \*BSD
* `coreutils`
	* `cp`
	* `cut`
	* `date`
	* `mkdir`
	* `mv`
	* `rm`
	* `rmdir`
	* `stat`
* `curl`
* `mkgmap`
	* will be downloaded when too old or missing
	* usually packaged as `mkgmap` and `mkgmap-splitter`
	* `splitter`
	* java 8
* `osmium`
	* usually packaged as `osmium-tool`
* `osmupdate`
	* usually packaged as `osmctools`
	* `gzip`
	* `osmconvert`
	* `wget`
* `sed`

### Optional

* `gdalbuildvrt`
	* usually packaged as `gdal` or `gdal-bin`
	* merge local dem data into single vrt file
	* required for `--dem-vrt`
* `gpsbabel`
	* required for `--gpi`
* `phyghtmap`
	* generate elevation OSM contour lines from elevation data (DEM)
	* required for `--dem` and `--dem-vrt`
	* `python3`
	* `python3-bs4`
	* `python3-lxml`
	* `python3-matplotlib`
	* `python3-numpy`
* `planetsplitter`
	* usually packaged as `routino`
	* build routing database of selection
	* required for `--routino`
* `python3-gdal`
	* for converting shape files to polygons with `util/ogr2poly.py`
	* required for specifying `--country` twice or more
	* `python3`
* `tput`
	* print some messages with colour

### External projects included in this repository

* Freizeitkarte style and typ
	* preprocessed into English to avoid perl and PPP dependency
	* modified POI text to show up in QMapShack for `*_poi.txt` variants
	* `asset/style/freizeitkarte`
	* `asset/typ/freizeitkarte_*.txt`
	* (c) FZK project, free for private use
	* http://www.freizeitkarte-osm.de/
* `util/ogr2poly.py`
	* ported to python3
	* modified to use output filenames in vermosm (lower_case_ascii) style
	* (c) Josh Doe, LGPLv3+
	* https://wiki.openstreetmap.org/wiki/Osmosis/Polygon_Filter_File_Format

### Services vermosm downloads data from

* Geofabrik (OSM data and region/single-country poly)
	* https://www.geofabrik.de/
* Geonames (geonames)
	* http://www.geonames.org/
* Natural Earth (multi-country shape files)
	* http://www.naturalearthdata.com/
* mkgmap (sea)
	* http://www.mkgmap.org.uk/
* Viewfinder Panoramas (DEM data)
	* http://www.viewfinderpanoramas.org/

## Updating assets

* clone freizeitkarte: https://github.com/freizeitkarte/fzk-mde-garmin/
* in `mt.pl` comment out `check_environment ();`
* modify an action so that it does only the following (in this example `build`)

```
create_typtranslations   ();
compile_typfiles         ();
create_typfile           ();
create_styletranslations ();
preprocess_styles        ();
```

* run `mt.pl` as usual: `./mt.pl --language=en build EUROPE`
* typ: `work/Freizeitkarte_EUROPE_en/*.txt`
* style: `work/Freizeitkarte_EUROPE_en/style/*`
