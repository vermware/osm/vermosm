gmap_geonames_age_max=31
gmap_geonames_url='https://download.geonames.org/export/dump/cities500.zip'
gmap_java_opt='-Xmx8192m'
gmap_sea_age_max=31
gmap_sea_url='http://osm.thkukuk.de/data/sea-latest.zip'

function gmap_opt_set()
{
	gmap_bounds_dir="$gmap_dir/vermosm_bounds"
	gmap_bounds_osm="$gmap_dir/vermosm_gmap_bounds.osm.pbf"
	gmap_geonames_zip="$gmap_dir/cities500.zip"
	gmap_gmapsupp_dir="$gmap_dir/vermosm_gmapsupp"
	gmap_licence="$gmap_dir/vermosm_licence.txt"
	gmap_sea_zip="$gmap_dir/sea.zip"
	gmap_tiles_dir="$gmap_dir/vermosm_tiles"
	gmap_typ_dir="$gmap_dir/typ"

	gmap_typ="${opt_typ##*/}"
	gmap_typ="${gmap_typ%.*}"
	gmap_typ="$gmap_typ_dir/vermosm_$gmap_typ.typ"

	gmap_description="v$VERMOSM_VERSION $(date --iso-8601=date), "
	gmap_description+="(c) $VERMOSM_YEAR $VERMOSM_DEV"

	unset gmap_country_name
	gmap_family_name="vermosm $opt_region"
	gmap_series_name="vermosm $opt_region"
	if [[ $opt_poly ]]
	then
		gmap_country_name="$poly_name"
		gmap_family_name+=" $poly_name"
		gmap_series_name+=" $poly_name"
	else
		gmap_country_name="${opt_country[*]}"
		gmap_family_name+=" ${opt_country[*]}"
		gmap_series_name+=" ${opt_country[*]}"
	fi

	gmap_gmapsupp="$gmap_dir/vermosm_$opt_region"
	if [[ $opt_poly ]]
	then
		gmap_gmapsupp+="_$poly_name"
	else
		for i in "${opt_country[@]}"
		do
			gmap_gmapsupp+="_$i"
		done
	fi
	gmap_gmapsupp+='_gmapsupp.img'

	unset gmap_mkdir_opt
	[[ $opt_verbose ]] && gmap_mkdir_opt='-v'

	unset gmap_mkgmap_redirect
	[[ ! $opt_verbose ]] && gmap_mkgmap_redirect='&> /dev/null'

	unset gmap_mv_opt
	[[ $opt_verbose ]] && gmap_mv_opt='-v'

	unset gmap_rm_opt
	[[ $opt_verbose ]] && gmap_rm_opt='-v'

	gmap_osmium_opt='--progress'
	[[ $opt_verbose ]] && gmap_osmium_opt='-v'
	[[ $opt_quiet ]] && gmap_osmium_opt='--no-progress'

	unset gmap_splitter_redirect
	[[ ! $opt_verbose ]] && gmap_splitter_redirect='&> /dev/null'

	return 0
}

function gmap_bounds_mk()
{
	msg "filter osm bounds -> $gmap_bounds_osm"
	# support deprecated ways to mark boundaries
	# https://wiki.openstreetmap.org/wiki/Relation:boundary
	osmium tags-filter \
		$gmap_osmium_opt \
		-o "$gmap_bounds_osm" \
		"$osm_extract" \
		'wra/boundary' \
		'wra/type=boundary,multipolygon' \
		'wra/role=inner,outer,' \
		'wra/admin_level' \
		'wra/postal_code'

	mkdir $gmap_mkdir_opt -p "$gmap_bounds_dir"

	msg "generate bounds -> $gmap_bounds_dir"
	set +e
	eval "java $gmap_java_opt -cp '$dep_mkgmap' \
		uk.me.parabola.mkgmap.reader.osm.boundary.BoundaryPreprocessor \
		'$gmap_bounds_osm' \
		'$gmap_bounds_dir' \
		$gmap_mkgmap_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in mkgmap -> run with verbose'
		return 1
	fi
	set -e

	return 0
}

function gmap_bounds_rm()
{
	[[ $opt_keep ]] && return 0

	msg_v 'remove gmap bounds tmp files'
	rm $gmap_rm_opt -r "$gmap_bounds_dir" "$gmap_bounds_osm"

	return 0
}

function gmap_geonames_download()
{
	if [[ -e $gmap_geonames_zip ]]
	then
		io_f_age_mk "$gmap_geonames_zip"
		if (( $io_f_age <= $gmap_geonames_age_max ))
		then
			msg_v "geonames $io_f_age days old -> skip download"
			return 0
		else
			msg_v "geonames $io_f_age days old -> update"
		fi
	fi

	msg "download geonames -> $gmap_geonames_zip"
	io_dl "$gmap_geonames_url" "$gmap_geonames_zip"

	return 0
}

function gmap_gmapsupp_mk()
{
	mkdir $gmap_mkdir_opt -p "$gmap_gmapsupp_dir"

	msg "build gmapsupp image -> $gmap_gmapsupp"
	set +e
	eval "java $gmap_java_opt -jar '$dep_mkgmap' \
		--gmapsupp \
		--output-dir='$gmap_gmapsupp_dir' \
		--country-name='$gmap_country_name' \
		--country-abbr='' \
		--region-name='$opt_region' \
		--region-abbr='' \
		--unicode \
		--lower-case \
		--index \
		--split-name-index \
		--bounds='$gmap_bounds_dir' \
		--location-autofill='is_in,nearest' \
		--housenumbers \
		--remove-ovm-work-files \
		--style-file='$opt_style' \
		--check-styles \
		--family-name='$gmap_family_name' \
		--series-name='$gmap_series_name' \
		--copyright-file='$gmap_licence' \
		--license-file='$gmap_licence' \
		--reduce-point-density=4 \
		--reduce-point-density-polygon=8 \
		--merge-lines \
		--max-jobs='$opt_jobs' \
		--route \
		--drive-on='detect,right' \
		--check-roundabouts \
		--add-pois-to-areas \
		--precomp-sea='$gmap_sea_zip' \
		--generate-sea \
		--link-pois-to-ways \
		--process-destination \
		--process-exits \
		--ignore-fixme-values \
		--poi-address \
		--verbose \
		--order-by-decreasing-area \
		--read-config='$gmap_tiles_dir/template.args' \
		--description='$gmap_description' \
		'$gmap_typ' \
		$gmap_mkgmap_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in mkgmap -> run with verbose'
		return 1
	fi
	set -e

	mv $gmap_mv_opt "$gmap_gmapsupp_dir/gmapsupp.img" "$gmap_gmapsupp"

	return 0
}

function gmap_gmapsupp_rm()
{
	[[ $opt_keep ]] && return 0

	msg_v 'remove gmapsupp tmp files'
	rm $gmap_rm_opt -r "$gmap_gmapsupp_dir"

	return 0
}

function gmap_licence_mk()
{
	# note that only the second line is displayed on the device
	# at least for Garmin Zumo 660

	msg "generate licence -> $gmap_licence"

	printf '%s\n%s\n%s %s\n' \
		'Copyright:' \
		"$VERMOSM_YEAR $VERMOSM_DEV (AGPLv3)" \
		"$(date +%Y)" \
		'OpenStreetMap contributors (ODbL)' \
		> "$gmap_licence"

	[[ $opt_dem ]] && \
		printf '%s\n' \
		'2014 Jonathan de Ferranti (private/research use)' \
		>> "$gmap_licence"

	return 0
}

function gmap_licence_rm()
{
	[[ $opt_keep ]] && return 0

	msg_v 'remove licence tmp file'
	rm $gmap_rm_opt "$gmap_licence"

	return 0
}

function gmap_sea_download()
{
	if [[ -e $gmap_sea_zip ]]
	then
		io_f_age_mk "$gmap_sea_zip"
		if (( $io_f_age <= $gmap_sea_age_max ))
		then
			msg_v "sea $io_f_age days old -> skip download"
			return 0
		else
			msg_v "sea $io_f_age days old -> update"
		fi
	fi

	msg "download sea -> $gmap_sea_zip"
	io_dl "$gmap_sea_url" "$gmap_sea_zip"

	return 0
}

function gmap_tiles_mk()
{
	mkdir $gmap_mkdir_opt -p "$gmap_tiles_dir"

	msg "split osm extract into tiles -> $gmap_tiles_dir"
	set +e
	eval "java $gmap_java_opt -jar '$dep_splitter' \
		--description='$gmap_description' \
		--geonames-file='$gmap_geonames_zip' \
		--max-areas=8192 \
		--max-nodes=750000 \
		--max-threads='$opt_jobs' \
		--output=pbf \
		--output-dir='$gmap_tiles_dir' \
		--precomp-sea='$gmap_sea_zip' \
		--search-limit=10000000 \
		--wanted-admin-level=11 \
		'$dem_osm_merged' \
		$gmap_splitter_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in mkgmap -> run with verbose'
		return 1
	fi
	set -e

	return 0
}

function gmap_tiles_rm()
{
	[[ $opt_keep ]] && return 0

	msg_v 'remove gmap tiles tmp files'
	rm $gmap_rm_opt -r "$gmap_tiles_dir"

	return 0
}

function gmap_typ_mk()
{
	mkdir $gmap_mkdir_opt -p "$gmap_typ_dir"

	# compile bundled first, as a given typ with the same name must override
	[[ $opt_typ_bundled ]] && \
		msg "compile bundled typ -> $gmap_typ_dir" && \
		for i in asset/typ/*.txt
	do
		out="${i##*/}"
		out="${out%.*}"
		out="$gmap_typ_dir/vermosm_$out.typ"

		msg_v "compile bundled typ -> $out"
		set +e
		eval "java $gmap_java_opt -cp '$dep_mkgmap' \
			uk.me.parabola.mkgmap.main.TypCompiler \
			'$i' \
			'$out' \
			$gmap_mkgmap_redirect"

		if (( $? != 0 ))
		then
			set -e
			[[ ! $opt_verbose ]] && \
				msg_e 'error in mkgmap -> run with verbose'
			return 1
		fi
		set -e

		unset out
	done

	msg "compile typ -> $gmap_typ"
	set +e
	eval "java $gmap_java_opt -cp '$dep_mkgmap' \
		uk.me.parabola.mkgmap.main.TypCompiler \
		'$opt_typ' \
		'$gmap_typ' \
		$gmap_mkgmap_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in mkgmap -> run with verbose'
		return 1
	fi
	set -e

	return 0
}

function gmap_typ_rm()
{
	[[ $opt_keep ]] && return 0

	msg_v 'remove typ tmp files'
	rm $gmap_rm_opt -r "$gmap_typ_dir"

	return 0
}
