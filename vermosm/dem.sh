function dem_opt_set()
{
	if [[ $opt_dem ]]
	then
		dem_osm_merged="$dem_dir/vermosm_dem_osm_merged.osm.pbf"
		dem_prefix="$dem_dir/vermosm_dem_tmp"
		dem_postfix='.osm.pbf'

		dem_osmium_opt='--progress'
		[[ $opt_verbose ]] && dem_osmium_opt='-v'
		[[ $opt_quiet ]] && dem_osmium_opt='--no-progress'

		unset dem_rm_opt
		[[ $opt_verbose ]] && dem_rm_opt='-v'
	else
		dem_osm_merged="$osm_extract"
	fi

	if [[ $opt_dem_vrt ]]
	then
		dem_vrt="$dem_dir/vermosm_dem_vrt.vrt"

		unset dem_gdalbuildvrt_opt
		[[ $opt_quiet ]] && dem_gdalbuildvrt_opt='-q'
	fi

	if [[ $opt_dem || $opt_dem_vrt ]]
	then
		unset dem_phyghtmap_redirect
		[[ ! $opt_verbose ]] && dem_phyghtmap_redirect='&> /dev/null'
	fi

	return 0
}

function dem_download()
{
	[[ ! $opt_dem && ! $opt_dem_vrt ]] && return 0

	msg "download dem data -> $dem_dir"
	set +e
	eval "phyghtmap \
		--polygon='$poly_merged' \
		--download-only \
		-j '$opt_jobs' \
		--source='view3' \
		--hgtdir='$dem_dir' \
		$dem_phyghtmap_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in phyghtmap -> run with verbose'
		return 1
	fi
	set -e

	return 0
}

function dem_mk()
{
	[[ ! $opt_dem ]] && return 0

	msg "build osm contour with dem data -> ${dem_prefix}_*$dem_postfix"
	set +e
	eval "phyghtmap \
		--polygon='$poly_merged' \
		-o '$dem_prefix' \
		-j '$opt_jobs' \
		--start-node-id=10000000000 \
		--start-way-id=15000000000 \
		--pbf \
		--source='view3' \
		--hgtdir='$dem_dir' \
		$dem_phyghtmap_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in phyghtmap -> run with verbose'
		return 1
	fi
	set -e

	return 0
}

function dem_rm()
{
	[[ ! $opt_dem || $opt_keep ]] && return 0

	# avoid calling rm *
	if [[ ! $dem_prefix || ! $dem_postfix ]]
	then
		msg_e 'BUG dem_prefix and/or dem_postfix not set'
		return 1
	fi

	msg_v 'remove dem contour tmp files'
	rm $dem_rm_opt "$dem_prefix"_*"$dem_postfix"

	return 0
}

function dem_osm_merged_mk()
{
	[[ ! $opt_dem ]] && return 0

	msg "merge osm extract with contour osm -> $dem_osm_merged"
	osmium merge \
		$dem_osmium_opt \
		-o "$dem_osm_merged" \
		"$osm_extract" "$dem_prefix"_*"$dem_postfix"

	return 0
}

function dem_osm_merged_rm()
{
	[[ ! $opt_dem || $opt_keep ]] && return 0

	msg_v 'remove osm extract contour osm merged tmp file'
	rm $dem_rm_opt "$dem_osm_merged"

	return 0
}

function dem_vrt_mk()
{
	[[ ! $opt_dem_vrt ]] && return 0

	msg "merge local dem files into vrt -> $dem_vrt"
	gdalbuildvrt \
		$dem_gdalbuildvrt_opt \
		"$dem_vrt" \
		"$dem_dir/VIEW3/"*

	return 0
}
