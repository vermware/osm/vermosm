opt_build_dir='build'
unset opt_country
declare -a opt_country
opt_country_len=0
unset opt_dem
unset opt_dem_vrt
unset opt_gpi
unset opt_install
opt_jobs="$(nproc)"
unset opt_keep
unset opt_poly
unset opt_parallel
unset opt_quiet
unset opt_region
unset opt_routino
opt_style='asset/style/freizeitkarte'
opt_typ='asset/typ/freizeitkarte_freizeit.txt'
unset opt_typ_bundled
unset opt_verbose

function opt_help()
{
	printf '%s\n' \
"$0 [OPTION]...
	vermosm / flexible OSM-based map generation

-b, --build-dir <dir>
	set build directory
	defaults to '$opt_build_dir'

-c, --country <country>
	country to build for
	specify multiple times to build for multiple countries
	all countries must be inside the specified region
	overrides --poly
	BUG cross-border routing has serious problems, prefer --poly (issue #12)

-C, --config <cfg>
	vermosm.cfg-style file to source
	overrides built-in defaults
	overrides system-wide vermosm.cfg options (stored next to vermosm.sh)
	overrides command-line options passed before, but not after, this option
	can be used multiple times in complex setups, above rules apply
	--config dump will print config and exit (pass as final option if used)

-d, --dem
	download and merge elevation data (DEM) as contour lines on the map
	on larger multi-country maps this is quite slow

-D, --dem-vrt
	merge all locally downloaded elevation data (DEM) into a single vrt
	the current region and country selection will be downloaded if needed
	all previous downloads in the same build directory will also be included

-g, --gpi
	generate a garmin-compatible gpi file containing all enforcement related
	nodes as waypoints suitable for the proximity/alert system
	generated from the full region, unless a single country is selected
	WARNING use of this file may not be legal in some countries

-h, --help
	print this message and exit

-i, --install <dir>
	install build files to <dir>
	files that already exist will be updated (overwritten)
	only selected options will be copied, dirs made if needed
	<dir>/dem/vermosm_dem_vrt.vrt and the VIEW3 folder
	<dir>/gpi/vermosm_*.gpi
	<dir>/map/vermosm_region_country..._gmapsupp.img
	<dir>/routino/vermosm_region_country...-*.mem
	<dir>/typ/vermosm_*.typ

-j, --jobs <jobs>
	number of jobs to run simultaneously in applications called by vermosm
	set to a low value if you run out of memory during build
	defaults to nproc which on this machine is '$opt_jobs'

-k, --keep
	keep *vermosm_* tmp files in the build directory
	they will be removed next build to prevent old data from being merged

-p, --poly <poly>
	poly file to build for, the name will be read from the file's first line
	the area specified in the polygon must be inside the specified region
	single-route gpx named routes supported, make sure extension is .gpx
	overrides --country

-P, --parallel
	run applications at the same time if possible, not related to --jobs
	for example, extracting osm region and dem contour lines run together
	this option may use up to two times as much memory as usual

-q, --quiet
	print only errors
	overrides --verbose

-r, --region <region>
	parent region for specified country or poly
	if neither of the above are specified build the entire region

-R, --routino <region|region-slim|selection|selection-slim>
	build a routino database for the specified region or selection
	*-slim uses much less memory but needs more time

-s, --style <style>
	file or directory to obtain map style from
	view mkgmap's --style-file option for more information
	defaults to '$opt_style'

-t, --typ <typ>
	typ file, in text form, to obtain extra bitmaps from
	view mkgmap's typ compiler manual and openstreetmap wiki for details
	defaults to '$opt_typ'

-T, --typ-bundled
	compile all vermosm bundled typ files in addition to --typ
	used in conjunction with --install to install all bundled typ files

-v, --verbose
	print what the script is doing
	overrides --quiet

-V, --version
	print version, copyright, licence information and where
	to find additional documentation and source code, then exit

--
	end of options
	any option after this is silently ignored"

	return 0
}

function opt_parse()
{
	while [[ $1 ]]
	do
		case "$1" in
		-b|--build-dir)
			opt_build_dir="$2"
			shift
			;;
		-c|--country)
			opt_country+=("$2")
			(( ++opt_country_len ))
			unset opt_poly
			shift
			;;
		-C|--config)
			[[ "$2" == 'dump' ]] && opt_print force && exit 0
			source "$2"
			shift
			;;
		-d|--dem)
			opt_dem=y
			;;
		-D|--dem-vrt)
			opt_dem_vrt=y
			;;
		-g|--gpi)
			opt_gpi=y
			;;
		-h|--help)
			opt_help
			exit 0
			;;
		-i|--install)
			opt_install="$2"
			shift
			;;
		-j|--jobs)
			opt_jobs="$2"
			shift
			;;
		-k|--keep)
			opt_keep=y
			;;
		-p|--poly)
			opt_poly="$2"
			unset opt_country
			declare -a opt_country
			opt_country_len=0
			shift
			;;
		-P|--parallel)
			opt_parallel=y
			;;
		-q|--quiet)
			opt_quiet=y
			unset opt_verbose
			;;
		-r|--region)
			opt_region="$2"
			shift
			;;
		-R|--routino)
			opt_routino="$2"
			shift
			;;
		-s|--style)
			opt_style="$2"
			shift
			;;
		-t|--typ)
			opt_typ="$2"
			shift
			;;
		-T|--typ-bundled)
			opt_typ_bundled=y
			;;
		-v|--verbose)
			opt_verbose=y
			unset opt_quiet
			;;
		-V|--version)
			opt_version
			exit 0
			;;
		--)
			break
			;;
		*)
			msg_e "invalid option -- '$1'"
			return 1
			;;
		esac
		shift
	done

	unset opt_parse_error

	if [[ -e $opt_build_dir && ! -d $opt_build_dir ]]
	then
		msg_e '--build target exists but is not a directory'
		opt_parse_error=y
	fi

	if [[ -e $opt_install && ! -d $opt_install ]]
	then
		msg_e '--install target exists but is not a directory'
		opt_parse_error=y
	fi

	if [[ $opt_poly && ! -f $opt_poly ]]
	then
		msg_e '--poly target is not a file'
		opt_parse_error=y
	fi

	if [[ ! $opt_region ]]
	then
		msg_e '--region is required'
		opt_parse_error=y
	fi

	if [[ $opt_routino && \
		$opt_routino != 'region' && \
		$opt_routino != 'region-slim' && \
		$opt_routino != 'selection' && \
		$opt_routino != 'selection-slim' ]]
	then
		msg_e '%s %s\n' '--routino is not one of' \
			'region, region-slim, selection, selection-slim'
		opt_parse_error=y
	fi

	if [[ $opt_style && ! -f $opt_style && ! -d $opt_style ]]
	then
		msg_e '--style target is not a file or directory'
		opt_parse_error=y
	fi

	if [[ $opt_typ && ! -f $opt_typ ]]
	then
		msg_e '--typ target is not a file'
		opt_parse_error=y
	fi

	if [[ $opt_parse_error ]]
	then
		msg_e "for help run '$0 --help'"
		msg_e
		opt_version
		return 1
	fi

	return 0
}

function opt_print()
{
	if [[ "$1" != 'force' ]]
	then
		[[ ! $opt_verbose ]] && return 0
		[[ $opt_quiet ]] && return 0
	fi

	printf 'options:\n'
	printf '\t%-11s = %s\n' 'build_dir' "$opt_build_dir"
	printf '\t%-11s = %s\n' 'country' "${opt_country[*]}"
	printf '\t%-11s = %s\n' 'dem' "$opt_dem"
	printf '\t%-11s = %s\n' 'dem_vrt' "$opt_dem_vrt"
	printf '\t%-11s = %s\n' 'gpi' "$opt_gpi"
	printf '\t%-11s = %s\n' 'install' "$opt_install"
	printf '\t%-11s = %s\n' 'jobs' "$opt_jobs"
	printf '\t%-11s = %s\n' 'keep' "$opt_keep"
	printf '\t%-11s = %s\n' 'poly' "$opt_poly"
	printf '\t%-11s = %s\n' 'parallel' "$opt_parallel"
	printf '\t%-11s = %s\n' 'quiet' "$opt_quiet"
	printf '\t%-11s = %s\n' 'region' "$opt_region"
	printf '\t%-11s = %s\n' 'routino' "$opt_routino"
	printf '\t%-11s = %s\n' 'style' "$opt_style"
	printf '\t%-11s = %s\n' 'typ' "$opt_typ"
	printf '\t%-11s = %s\n' 'typ_bundled' "$opt_typ_bundled"
	printf '\t%-11s = %s\n' 'verbose' "$opt_verbose"

	return 0
}

function opt_version()
{
	printf '%s\n' \
"vermosm $VERMOSM_VERSION / $VERMOSM_DATE
copyright (c) $VERMOSM_YEAR $VERMOSM_DEV
licensed under GNU AGPLv3 <https://www.gnu.org/licenses/agpl-3.0.html>
additional documentation and sources <$VERMOSM_URL>"

	return 0
}
