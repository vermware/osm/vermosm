function routino_opt_set()
{
	[[ ! $opt_routino ]] && return 0

	routino_db_prefix_opt="vermosm_$opt_region"

	if [[ $opt_routino == 'region'* ]]
	then
		routino_osm="$osm_region"
	else
		routino_osm="$osm_extract"
		if [[ $opt_poly ]]
		then
			routino_db_prefix_opt+="_$poly_name"
		else
			for i in "${opt_country[@]}"
			do
				routino_db_prefix_opt+="_$i"
			done
		fi
	fi

	if [[ $opt_routino == *'-slim' ]]
	then
		routino_planetsplitter=planetsplitter-slim
	else
		routino_planetsplitter=planetsplitter
	fi

	routino_db_prefix="$routino_dir/$routino_db_prefix_opt-"
	routino_db_postfix='.mem'

	unset routino_planetsplitter_redirect
	[[ ! $opt_verbose ]] && routino_planetsplitter_redirect='&> /dev/null'

	return 0
}

function routino_db_mk()
{
	[[ ! $opt_routino ]] && return 0

	msg 'build routino database -> %s\n' \
		"$routino_db_prefix*$routino_db_postfix"
	set +e
	eval "$routino_planetsplitter \
		--dir='$routino_dir' \
		--prefix='$routino_db_prefix_opt' \
		'$routino_osm' \
		$routino_planetsplitter_redirect"

	if (( $? != 0 ))
	then
		set -e
		[[ ! $opt_verbose ]] && \
			msg_e 'error in planetsplitter -> run with verbose'
		return 1
	fi
	set -e

	return 0
}
