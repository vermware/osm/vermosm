unset dep_mkgmap
dep_mkgmap_url='https://www.mkgmap.org.uk/download/mkgmap-latest.zip'
dep_mkgmap_ver_min=3997
unset dep_splitter
dep_splitter_url='https://www.mkgmap.org.uk/download/splitter-latest.zip'
dep_splitter_ver_min=584

function dep_opt_set()
{
	unset dep_mkdir_opt
	[[ $opt_verbose ]] && dep_mkdir_opt='-v'

	unset dep_mv_opt
	[[ $opt_verbose ]] && dep_mv_opt='-v'

	unset dep_rmdir_opt
	[[ $opt_verbose ]] && dep_rmdir_opt='-v'

	dep_unzip_opt='-q'
	[[ $opt_quiet ]] && dep_unzip_opt='-qq'
	[[ $opt_verbose ]] && unset dep_unzip_opt

	return 0
}

function dep_gdalbuildvrt()
{
	[[ ! $opt_dem_vrt ]] && return 0

	msg_v 'check dependency gdalbuildvrt'
	if [[ ! $(command -v gdalbuildvrt) ]]
	then
		msg_e 'failed to find gdalbuildvrt'
		return 1
	fi

	return 0
}

function dep_gpsbabel()
{
	[[ ! $opt_gpi ]] && return 0

	msg_v 'check dependency gpsbabel'
	if [[ ! $(command -v gpsbabel) ]]
	then
		msg_e 'failed to find gpsbabel'
		return 1
	fi

	return 0
}


function dep_mkgmap()
{
	msg_v 'check dependency mkgmap'

	for i in \
		"$dep_dir/mkgmap/mkgmap.jar" \
		'/usr/share/mkgmap/mkgmap.jar' \
		'/usr/share/java/mkgmap/mkgmap.jar'
	do
		if [[ -e $i ]]
		then
			dep_mkgmap="$i"
			break
		fi
	done

	[[ ! $dep_mkgmap ]] && dep_mkgmap_download

	(( $(java -jar "$dep_mkgmap" --version 2>&1 | sed '$!d') \
		< $dep_mkgmap_ver_min )) && dep_mkgmap_download

	msg_v '\t%s\n' "$dep_mkgmap"

	return 0
}

function dep_mkgmap_download()
{
	msg 'mkgmap not installed or too old -> download mkgmap'

	io_dl "$dep_mkgmap_url" "$dep_dir/mkgmap.zip"
	mkdir $dep_mkdir_opt -p "$dep_dir/mkgmap"
	unzip $dep_unzip_opt -o "$dep_dir/mkgmap.zip" -d "$dep_dir/mkgmap"
	mv $dep_mv_opt -- "$dep_dir/mkgmap/mkgmap-r"*/* "$dep_dir/mkgmap"
	rmdir $dep_rmdir_opt "$dep_dir/mkgmap/mkgmap-r"*

	dep_mkgmap="$dep_dir/mkgmap/mkgmap.jar"

	return 0
}

function dep_planetsplitter()
{
	[[ ! $opt_routino ]] && return 0

	msg_v 'check dependency planetsplitter'
	if [[ ! $(command -v planetsplitter) ]]
	then
		msg_e 'failed to find planetsplitter'
		return 1
	fi

	return 0
}

function dep_splitter()
{
	msg_v 'check dependency splitter'

	for i in \
		"$dep_dir/splitter/splitter.jar" \
		'/usr/share/mkgmap-splitter/splitter.jar' \
		'/usr/share/java/splitter/splitter.jar'
	do
		if [[ -e $i ]]
		then
			dep_splitter="$i"
			break
		fi
	done

	[[ ! $dep_splitter ]] && dep_splitter_download

	(( $(java -jar "$dep_splitter" --version 2>&1 | \
		sed '$!d' | cut -d ' ' -f 2) \
		< $dep_splitter_ver_min )) && dep_splitter_download

	msg_v '\t%s\n' "$dep_splitter"

	return 0
}

function dep_splitter_download()
{
	msg 'splitter not installed or too old -> download splitter'

	io_dl "$dep_splitter_url" "$dep_dir/splitter.zip"
	mkdir $dep_mkdir_opt -p "$dep_dir/splitter"
	unzip $dep_unzip_opt -o "$dep_dir/splitter.zip" -d "$dep_dir/splitter"
	mv $dep_mv_opt -- "$dep_dir/splitter/splitter-r"*/* "$dep_dir/splitter"
	rmdir $dep_rmdir_opt "$dep_dir/splitter/splitter-r"*

	dep_splitter="$dep_dir/splitter/splitter.jar"

	return 0
}
