osm_mirror="https://download.geofabrik.de"

function osm_opt_set()
{
	osm_region="$osm_dir/$opt_region.osm.pbf"
	if [[ $opt_poly ]]
	then
		osm_extract="$osm_dir/vermosm_${opt_region}_$poly_name.osm.pbf"
	elif (( $opt_country_len == 0 ))
	then
		osm_extract="$osm_region"
	elif (( $opt_country_len == 1 ))
	then
		osm_extract="$osm_dir/${opt_country[0]}.osm.pbf"
	else
		osm_extract="$osm_dir/vermosm_osm_extract.osm.pbf"
	fi

	osm_gpi_dir="$osm_dir/gpi"
	if (( $opt_country_len == 1 ))
	then
		osm_gpi="$osm_gpi_dir"
		osm_gpi+="/vermosm_${opt_region}_${opt_country[0]}.gpi"
		osm_gpi_src="$osm_dir/${opt_country[0]}.osm.pbf"
	else
		osm_gpi="$osm_gpi_dir/vermosm_$opt_region.gpi"
		osm_gpi_src="$osm_region"
	fi

	unset osm_mkdir_opt
	[[ $opt_verbose ]] && osm_mkdir_opt='-v'

	unset osm_mv_opt
	[[ $opt_verbose ]] && osm_mv_opt='-v'

	osm_osmium_opt='--progress'
	[[ $opt_verbose ]] && osm_osmium_opt='-v'
	[[ $opt_quiet ]] && osm_osmium_opt='--no-progress'

	unset osm_osmupdate_opt
	[[ $opt_verbose ]] && osm_osmupdate_opt='-v'

	unset osm_osmupdate_redirect
	[[ $opt_quiet ]] && osm_osmupdate_redirect='&> /dev/null'

	unset osm_rm_opt
	[[ $opt_verbose ]] && osm_rm_opt='-v'

	return 0
}

function osm_download()
{
	if [[ $opt_poly ]]
	then
		msg_v 'poly specified as option -> region osm'
		osm_download_region
	elif (( $opt_country_len == 1 ))
	then
		msg_v '1 country selected -> country osm'
		osm_download_country
	else
		msg_v "$opt_country_len countries selected -> region osm"
		osm_download_region
	fi

	return 0
}

function osm_download_country()
{
	osm_out="$osm_dir/${opt_country[0]}.osm.pbf"

	if [[ -e $osm_out ]]
	then
		osm_url="$osm_mirror/$opt_region/${opt_country[0]}-updates"
		msg "updating osm country -> $osm_out"
		osm_state_parse
		osm_diff_download
		osm_diff_apply
	else
		osm_url="$osm_mirror/$opt_region/"
		osm_url+="${opt_country[0]}-latest.osm.pbf"
		msg_v "download osm country -> $osm_out"
		io_dl "$osm_url" "$osm_out"
	fi

	return 0
}

function osm_download_region()
{
	osm_out="$osm_region"

	if [[ -e $osm_out ]]
	then
		osm_url="$osm_mirror/$opt_region-updates"
		msg "updating osm region -> $osm_out"
		osm_state_parse
		osm_diff_download
		osm_diff_apply
	else
		osm_url="$osm_mirror/$opt_region-latest.osm.pbf"
		msg_v "download osm region -> $osm_out"
		io_dl "$osm_url" "$osm_out"
	fi

	return 0
}

function osm_extract_mk()
{
	(( $opt_country_len <= 1 )) && [[ ! $opt_poly ]] && return 0

	msg "extract osm region with poly -> $osm_extract"
	osmium extract \
		$osm_osmium_opt \
		-p "$poly_merged" \
		-s 'smart' \
		-o "$osm_extract" \
		"$osm_region"

	return 0
}

function osm_extract_rm()
{
	[[ $opt_keep ]] && return 0
	(( $opt_country_len <= 1 )) && [[ ! $opt_poly ]] && return 0

	msg_v 'remove osm extract tmp file'
	rm $osm_rm_opt "$osm_extract"

	return 0
}

function osm_state_parse()
{
	msg_v "parse osm state -> $osm_url/state.txt"
	osm_state="$(curl -sSLf "$osm_url/state.txt")"

	osm_time="$(echo "$osm_state" \
		| grep '^timestamp=' \
		| sed -e 's/.*=//' -e 's/\\//g')"
	osm_seq="$(echo "$osm_state" \
		| grep '^sequenceNumber=' \
		| sed -e 's/.*=//')"

	msg_v '\t%-8s = %s\n' 'osm_time' "$osm_time"
	msg_v '\t%-8s = %s\n' 'osm_seq' "$osm_seq"

	return 0
}

function osm_diff_apply()
{
	if [[ ! -e "$osm_diff" ]]
	then
		msg_v 'no osm diff found -> skip apply diff'
		return 0
	fi

	osm_out_new="${osm_out%.osm.pbf}_vermosm_osm_new.osm.pbf"

	msg_v "apply osm diff -> $osm_out_new"
	osmium apply-changes \
		$osm_osmium_opt \
		-o "$osm_out_new" \
		--output-header="osmosis_replication_timestamp=$osm_time" \
		--output-header="osmosis_replication_sequence_number=$osm_seq" \
		--output-header="osmosis_replication_base_url=$osm_url" \
		"$osm_out" "$osm_diff"

	mv $osm_mv_opt "$osm_out_new" "$osm_out"

	rm $osm_rm_opt "$osm_diff"

	return 0
}

function osm_diff_download()
{
	osm_diff="${osm_out%.osm.pbf}_vermosm_osm_diff.osc.gz"

	msg_v "download osm diff -> $osm_diff"
	set +e
	eval "osmupdate \
		$osm_osmupdate_opt \
		--tempfiles='$osm_dir/vermosm_tmp' \
		--base-url='$osm_url' \
		'$osm_out' '$osm_diff' \
		$osm_osmupdate_redirect"

	# osmupdate exits with 21 when already up-to-date
	if (( $? != 0 && $? != 21 ))
	then
		set -e
		[[ $opt_quiet ]] && \
			msg_e 'error in osmupdate -> run without quiet'
		return 1
	fi
	set -e

	return 0
}

function osm_gpi_mk()
{
	[[ ! $opt_gpi ]] && return 0

	msg "generate gpi -> $osm_gpi"
	mkdir $osm_mkdir_opt -p "$osm_gpi_dir"

	osmium tags-filter \
		$osm_osmium_opt \
		-o "$osm_gpi_dir/vermosm_filter.osm" \
		"$osm_gpi_src" \
		'/highway=speed_camera' \
		'/enforcement'

	gpsbabel \
		-i osm \
		-f "$osm_gpi_dir/vermosm_filter.osm" \
		-o garmin_gpi,alerts=1,category="vermosm",position=1,proximity=400m,units=m \
		-F "$osm_gpi"

	return 0
}

function osm_gpi_rm()
{
	[[ $opt_keep ]] && return 0
	[[ ! $opt_gpi ]] && return 0

	msg_v 'remove gpi tmp files'
	rm $osm_rm_opt -r "$osm_gpi_dir"

	return 0
}
