poly_mirror='https://download.geofabrik.de'
poly_shape_age_max=31
poly_shape_url='https://www.naturalearthdata.com/http//www.naturalearthdata.com'
poly_shape_url+='/download/10m/cultural/ne_10m_admin_0_countries.zip'

function poly_opt_set()
{
	unset poly_ogr2poly_opt
	[[ $opt_verbose ]] && poly_ogr2poly_opt='-v'

	unset poly_rm_opt
	[[ $opt_verbose ]] && poly_rm_opt='-v'

	poly_unzip_opt='-q'
	[[ $opt_quiet ]] && poly_unzip_opt='-qq'
	[[ $opt_verbose ]] && unset poly_unzip_opt

	unset poly_name
	unset poly_gpx
	if [[ ${opt_poly##*.} == 'gpx' ]]
	then
		poly_name="$(sed -En 's,.*<name>(.*)</name>.*,\1,p' \
				"$opt_poly" \
			| sed -En '$p')"
		poly_gpx="$poly_dir/vermosm_gpx.poly"
	elif [[ $opt_poly ]]
	then
		poly_name="$(sed -n 1p "$opt_poly")"
	fi

	return 0
}

function poly_download()
{
	if [[ $poly_gpx ]]
	then
		msg_v 'poly gpx specified as option -> converting to poly'
		{
		sed -En 's,.*<name>(.*)</name>.*,\1,p' "$opt_poly" \
			| sed -En '$p'
		printf '1\n'
		sed -En 's,.*<rtept[ \t]+(|.*[ \t]+)lon="(.*)"[ \t]+(|.*[ \t]+)lat="(.*)"(|[ \t]+.*)/?>.*,  \2 \4,p' \
			"$opt_poly"
		sed -En 's,.*<rtept[ \t]+(|.*[ \t]+)lat="(.*)"[ \t]+(|.*[ \t]+)lon="(.*)"(|[ \t]+.*)/?>.*,  \4 \2,p' \
			"$opt_poly"
		} > "$poly_gpx"
		printf '%s\nEND\nEND\n' \
			"$(sed -En '3p' "$poly_gpx")" \
			>> "$poly_gpx"
		return 0
	elif [[ $opt_poly ]]
	then
		msg_v 'poly specified as option -> skipping poly download'
		return 0
	fi

	if (( $opt_country_len == 0 ))
	then
		msg_v '0 countries selected -> region poly'
		poly_url="$poly_mirror/$opt_region.poly"
		poly_out="$poly_dir/$opt_region.poly"
		msg_v "$opt_region -> $poly_out"
		io_dl "$poly_url" "$poly_out"
	elif (( $opt_country_len == 1 ))
	then
		msg_v 'single country selected -> country poly'
		poly_url="$poly_mirror/$opt_region/${opt_country[0]}.poly"
		poly_out="$poly_dir/${opt_country[0]}.poly"
		msg_v "${opt_country[0]} -> $poly_out"
		io_dl "$poly_url" "$poly_out"
	else
		msg_v "$opt_country_len countries selected -> country polys"
		poly_download_shape
	fi

	return 0
}

function poly_download_shape()
{
	poly_shape_zip="$poly_dir/ne_10m_admin_0_countries.zip"

	if [[ -e $poly_shape_zip ]]
	then
		io_f_age_mk "$poly_shape_zip"
		if (( $io_f_age <= $poly_shape_age_max ))
		then
			msg_v "shape $io_f_age days old -> skip download"
			poly_download_shape_extract
			return 0
		else
			msg_v "shape $io_f_age days old -> update"
		fi
	fi

	msg "download shape -> $poly_shape_zip"
	io_dl "$poly_shape_url" "$poly_shape_zip"

	poly_download_shape_extract

	return 0
}

function poly_download_shape_extract()
{
	msg "unzip shape -> $poly_dir"
	unzip $poly_unzip_opt -o "$poly_shape_zip" -d "$poly_dir"

	msg "build poly from shape -> $poly_dir/vermosm_*.poly"
	util/ogr2poly.py \
		$poly_ogr2poly_opt \
		-p "$poly_dir/vermosm_" \
		-f geounit \
		"$poly_dir/ne_10m_admin_0_countries.dbf"

	return 0
}

function poly_rm()
{
	[[ $opt_keep ]] && return 0
	if [[ ! $poly_gpx ]]
	then
		[[ $opt_poly ]] && return 0
		(( $opt_country_len <= 1 )) && return 0
	fi

	if [[ ! $poly_dir ]]
	then
		msg_e 'BUG poly_dir not set'
		return 1
	fi

	rm $poly_rm_opt "$poly_dir/vermosm_"*'.poly'

	return 0
}

function poly_merged_mk()
{
	if [[ $poly_gpx ]]
	then
		msg_v 'poly gpx specified as option -> skipping poly merge'
		poly_merged="$poly_gpx"
		return 0
	elif [[ $opt_poly ]]
	then
		msg_v 'poly specified as option -> skipping poly merge'
		poly_merged="$opt_poly"
		return 0
	elif (( $opt_country_len <= 1 ))
	then
		msg_v 'region or single country selected -> skipping poly merge'
		poly_merged="$poly_out"
		return 0
	fi

	poly_merged="$poly_dir/vermosm_poly_merged.poly"
	msg "merge country polys -> $poly_merged"

	printf 'vermosm_poly_merged\n' > "$poly_merged"

	for i in "${opt_country[@]}"
	do
		msg_v '\t%s\n' "$poly_dir/vermosm_$i.poly"

		sed -e '1,2d' -e "3i$i" -e '$d' \
			< "$poly_dir/vermosm_$i.poly" \
			>> "$poly_merged"
	done

	printf 'END\n' >> "$poly_merged"

	return 0
}
