function io_opt_set
{
	dem_dir="$opt_build_dir/dem"
	dep_dir="$opt_build_dir/dep"
	gmap_dir="$opt_build_dir/gmap"
	osm_dir="$opt_build_dir/osm"
	poly_dir="$opt_build_dir/poly"
	routino_dir="$opt_build_dir/routino"

	unset io_curl_opt
	if [[ $opt_quiet ]]
	then
		io_curl_opt='-sS'
	elif [[ ! $opt_verbose ]]
	then
		io_curl_opt='-#'
	fi

	unset io_mkdir_opt
	[[ $opt_verbose ]] && io_mkdir_opt='-v'

	return 0
}

function io_build_dir_mk()
{
	mkdir $io_mkdir_opt -p \
		"$dem_dir" \
		"$dep_dir" \
		"$gmap_dir" \
		"$osm_dir" \
		"$poly_dir" \
		"$routino_dir"

	return 0
}

function io_build_dir_clean()
{
	# sanity check
	[[ -d $opt_build_dir ]] || return 1

	for i in "$opt_build_dir"/**/*vermosm_*
	do
		rm -rv -- "$i"
	done

	return 0
}

function io_f_age_mk()
{
	if [[ ! $1 ]]
	then
		msg_e 'io_file_age_days() missing opt file'
		return 1
	fi

	msg_v "get file age -> $1"
	io_f_age_epoch="$(date '+%s')"
	io_f_age_stat="$(stat -c '%Y' "$1")"
	io_f_age="$(( ($io_f_age_epoch - $io_f_age_stat) / 86400 ))"
	msg_v '\t(epoch[%s] - stat[%s]) / 86400 = days[%s]\n' \
		"$io_f_age_epoch" "$io_f_age_stat" "$io_f_age"

	return 0
}

function io_dl()
{
	if [[ ! $1 ]]
	then
		msg_e 'io_dl() missing opt url'
		return 1
	elif [[ ! $2 ]]
	then
		msg_e 'io_dl() missing opt out'
		return 1
	fi

	msg "$1"
	curl $io_curl_opt -Lfo "$2" "$1"

	return 0
}

function msg()
{
	[[ $opt_quiet ]] && return 0

	if (( $# <= 1 ))
	then
		printf '%s\n' "$1"
	else
		printf "$@"
	fi

	return 0
}

function msg_e()
{
	if (( $# <= 1 ))
	then
		printf '%s\n' "$1"
	else
		printf "$@"
	fi

	return 0
}

function msg_v()
{
	[[ ! $opt_verbose ]] && return 0

	if (( $# <= 1 ))
	then
		printf '%s\n' "$1"
	else
		printf "$@"
	fi

	return 0
}
