function install_opt_set()
{
	unset install_cp_opt
	[[ $opt_verbose ]] && install_cp_opt='-v'

	unset install_mkdir_opt
	[[ $opt_verbose ]] && install_mkdir_opt='-v'

	unset install_mv_opt
	[[ $opt_verbose ]] && install_mv_opt='-v'

	return 0
}

function install()
{
	[[ ! $opt_install ]] && return 0

	msg "install generated files -> $opt_install"

	if [[ $opt_dem_vrt ]]
	then
		mkdir $install_mkdir_opt -p \
			"$opt_install/dem"

		cp $install_cp_opt -rfu \
			"$dem_dir/VIEW3" \
			"$opt_install/dem"

		mv $install_mv_opt -f \
			"$dem_vrt" \
			"$opt_install/dem"
	fi

	if [[ $opt_gpi ]]
	then
		mkdir $install_mkdir_opt -p \
			"$opt_install/gpi"

		mv $install_mv_opt -f \
			"$osm_gpi" \
			"$opt_install/gpi"
	fi

	mkdir $install_mkdir_opt -p \
		"$opt_install/map"

	mv $install_mv_opt -f \
		"$gmap_gmapsupp" \
		"$opt_install/map"

	if [[ $opt_routino ]]
	then
		mkdir $install_mkdir_opt -p \
			"$opt_install/routino"

		if [[ ! $routino_db_prefix || ! $routino_db_postfix ]]
		then
			msg_e 'BUG routino_db_prefix/routino_db_postfix not set'
			return 1
		fi

		mv $install_mv_opt -f \
			"$routino_db_prefix"*"$routino_db_postfix" \
			"$opt_install/routino"
	fi

	mkdir $install_mkdir_opt -p \
		"$opt_install/typ"

	mv $install_mv_opt -f \
		"$gmap_typ_dir"/vermosm_*.typ \
		"$opt_install/typ"

	return 0
}
